#include <Wire.h>

#include <RtcDS3231.h>
RtcDS3231<TwoWire> Rtc(Wire);

const byte eeprom_address = 0x50;
uint64_t chipid;

void setup(void)
{
  // Start Serial
  Serial.begin(115200);

  Serial.println("Sketch for init RTC and EEPROM functionality and print info about ESP32 chip module");
  Serial.println("------------------------------------------------------------------------------\n\n");

  check_esp();
  
  Wire.begin(21, 22);
  i2c_scan();
  
  Rtc.Begin();
  rtc_init();
    
  init_eeprom_counter_array();
}


void loop() {

  Serial.println(">>>>>>>>>>>>> RTC <<<<<<<<<<<<<<<<<<<\n\n");
  if (!Rtc.IsDateTimeValid()) {
    Serial.println("RTC lost confidence in the DateTime!");
  }

  RtcDateTime now = Rtc.GetDateTime();
  printDateTime(now);
  Serial.println();

  RtcTemperature temp = Rtc.GetTemperature();
  Serial.print(temp.AsFloatDegC());
  Serial.println("C");
  Serial.println(">>>>>>>>>>>>> EEPROM <<<<<<<<<<<<<<<<<<<\n\n");
  check_eeprom_counter_array();
  delay(15000);
}

void check_esp() {
  uint32_t ideSize = ESP.getFlashChipSize();
  FlashMode_t ideMode = ESP.getFlashChipMode();

  Serial.println("========== ESP and Arduino IDE configuration info ===============\n\n");

  Serial.printf("Flash ide  size: %u\n", ideSize);
  Serial.printf("Flash ide speed: %u\n", ESP.getFlashChipSpeed());
  Serial.printf("Flash ide mode:  %s\n", (ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" : ideMode == FM_DIO ? "DIO" : ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));

  chipid=ESP.getEfuseMac();//The chip ID is essentially its MAC address(length: 6 bytes).
  Serial.printf("ESP32 Chip ID = %04X",(uint16_t)(chipid>>32));//print High 2 bytes
  Serial.printf("%08X\n",(uint32_t)chipid);//print Low 4bytes.

  Serial.println("------------------------------------------------------------------\n");
  
}

void i2c_scan() {

  byte error, address;
  int nDevices;
 
  Serial.println("=========== I2C bus scaning ======================================\n\n");
 
  nDevices = 0;
  for(address = 1; address < 127; address++ ) {
 
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0) {
      Serial.print("I2C device found at address 0x");
      if (address<16) 
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");
 
      nDevices++;
    }
    else if (error==4) 
    {
      Serial.print("Unknow error at address 0x");
      if (address<16) 
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");

  Serial.println("--------------------------------------------------------------------\n\n");
}

void rtc_init() {
  
    RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
    printDateTime(compiled);
    Serial.println("================ Init RTC and set date and time =================\n\n");

    if (!Rtc.IsDateTimeValid()) {
        Serial.println("RTC lost confidence in the DateTime!");
        Rtc.SetDateTime(compiled);
    }

    if (!Rtc.GetIsRunning()) {
        Serial.println("RTC was not actively running, starting now");
        Rtc.SetIsRunning(true);
    }

    RtcDateTime now = Rtc.GetDateTime();
    if (now < compiled) {
        Serial.println("RTC is older than compile time!  (Updating DateTime)");
        Rtc.SetDateTime(compiled);
    } else if (now > compiled) {
        Serial.println("RTC is newer than compile time. (this is expected)");
    } else if (now == compiled) {
        Serial.println("RTC is the same as compile time! (not expected but all is fine)");
    }

    Rtc.Enable32kHzPin(false);
    Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone); 
    Serial.println("------------------------------------------------------------------\n\n");
}

#define countof(a) (sizeof(a) / sizeof(a[0]))

void printDateTime(const RtcDateTime& dt)
{
    char datestring[20];

    snprintf_P(datestring, 
            countof(datestring),
            PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
            dt.Month(),
            dt.Day(),
            dt.Year(),
            dt.Hour(),
            dt.Minute(),
            dt.Second() );
    Serial.print(datestring);
}


#define countof(a) (sizeof(a) / sizeof(a[0]))


// First 36 address are organized in 18x2 counter array, actual counter
// may be determinatde by checking first pair of bytes
// MSB is set to B10101010 and counter position is store in LSB
void init_eeprom_counter_array() {
  Wire.beginTransmission(eeprom_address);
  Wire.write(0);
  Wire.write(0);
  Wire.write(B10101010);
  Wire.write(0);
  for ( uint8_t i = 2; i<18;i++) {
    Wire.write(8);
  }
  Wire.endTransmission();
  delay(50);
  Wire.beginTransmission(eeprom_address);
  Wire.write(0);
  Wire.write(18);
  for ( uint8_t i = 18; i<36;i++) {
    Wire.write(8);
  }
  Wire.endTransmission();
  delay(50);
}

void check_eeprom_counter_array() {
  byte rdata1, rdata2;
  char buffer[16];

  for ( uint8_t i = 0; i<18;i++) {
    Wire.beginTransmission(eeprom_address);
    Wire.write(0);
    Wire.write(i*2);
    Wire.endTransmission();
    delay(10); 
    Wire.requestFrom(eeprom_address, 2);
    if (Wire.available()) {
      rdata1 = Wire.read();    
      rdata2 = Wire.read();
      sprintf(buffer, "%2u. |%2X|%2X|", i,rdata1,rdata2);
      Serial.println(buffer);
    }
    Wire.endTransmission();
    delay(10);
  }
}
